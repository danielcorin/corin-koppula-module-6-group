from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from cal.api import CalResource, EventResource, UserResource

from tastypie.api import Api

# RESTful setup:
cal_resource = CalResource()
event_resource = EventResource()

# v1_api = Api(api_name='v1')
# v1_api.register(UserResource())
# v1_api.register(CalResource())
# v1_api.register(EventResource())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'cse330calendar.views.home', name='home'),
    # url(r'^cse330calendar/', include('cse330calendar.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # Templates
    url(r'^', include('cal.urls', namespace="cal"),),
    # url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'cal/login.html'}),
 
    # RESTful URLs
    url(r'^', include(cal_resource.urls)),
    url(r'^', include(event_resource.urls)),

    # url(r'^', include(v1_api.urls)),
)
