from tastypie.resources import ModelResource
from tastypie.paginator import Paginator
from tastypie import fields
from models import Cal, Event
from tastypie.fields import IntegerField
from tastypie.fields import ToOneField
from auth import CalAuthorization, EventAuthorization

from tastypie.authentication import BasicAuthentication
from tastypie.authorization import Authorization, ReadOnlyAuthorization

from django.contrib.auth.models import User

from tastypie.constants import ALL, ALL_WITH_RELATIONS




class UserResource(ModelResource):
	class Meta:
		queryset = User.objects.all()
		resource_name = 'user'
		excludes = ['email', 'password', 'is_active', 'is_staff', 'is_superuser']
		allowed_methods = ['get', 'post', 'put', 'delete']

# class CalResource(ModelResource):
# 	# user = fields.ForeignKey(UserResource, 'user')
# 	class Meta:
# 		queryset = Cal.objects.all()
# 		paginator_class = Paginator
 
# class EventResource(ModelResource):
# 	cal_id = IntegerField(attribute="cal__id") # load the calendar ID as an integer
# 	class Meta:
# 		queryset = Event.objects.all()
# 		paginator_class = Paginator
# 		authorization= Authorization()



 
class CalResource(ModelResource):
	class Meta:
		queryset = Cal.objects.all()
		paginator_class = Paginator
		authorization = CalAuthorization()
		user = fields.ForeignKey(UserResource, 'user')
		# authorization = UserObjectsOnlyAuthorization()
 
class EventResource(ModelResource):
	cid = ToOneField(CalResource, "cal", full=True)
	user = fields.ForeignKey(UserResource, 'user')
	class Meta:
		queryset = Event.objects.all()
		paginator_class = Paginator
		always_return_data = True
		authorization = EventAuthorization()

	# 	authorization = EventAuthorization()
	# 	queryset = Event.objects.all()
	# 	resource_name = 'goal'
	# 	filtering = {
	# 	  'user': ALL_WITH_RELATIONS,
	# }

	# def obj_create(self, bundle, request=None, **kwargs):
 #   		return super(EnvironmentResource, self).obj_create(bundle, request, user=request.user)


	# def apply_authorization_limits(self, request, object_list):
	# 	if request.user.is_superuser:
	# 		return object_list.filter(user__id=request.GET.get('user__id',''))
		

		# authorization = UserObjectsOnlyAuthorization()

	def dehydrate_cid(self, bundle):
		return bundle.obj.cal.id
	def hydrate_cid(self, bundle):
		bundle.data["cid"] = "/cal/%d/" % bundle.data["cid"]
		return bundle
	def hydrate_id(self, bundle):
		if bundle.data["id"] == 0:
			bundle.data["id"] = None
		return bundle
	def obj_create(self, bundle, **kwargs):
		return super(EventResource, self).obj_create(bundle, user=bundle.request.user)




