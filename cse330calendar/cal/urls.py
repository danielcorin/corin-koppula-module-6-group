from django.conf.urls import patterns, url
import views


urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.login_view, name='login_view'),
    url(r'^logout/$', views.logout_view, name='logout_view'),
    url(r'^register/$', views.register_view, name='register_view'),
    url(r'^add_cal/$', views.add_cal_view, name='add_cal_view'),

)