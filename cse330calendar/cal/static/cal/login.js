Ext.fly("loginForm").on("submit", function(event, element){
	event.preventDefault();
	Ext.Ajax.request({
		form: element,
		url: '/login/',
		success: function(response) {
			console.log("Success", response);
			var data = JSON.parse(response.responseText);
			if(data.success)
			{
				Ext.getElementById("message").innerHTML = "";
				Ext.fly("logoutForm").show();
				Ext.fly("addCalForm").show();
				Ext.fly("loginForm").hide();
				Ext.fly("registerForm").hide();
				Ext.getElementById("user").innerHTML = data.username;
				Ext.getElementById("loginForm").reset();
				Ext.getElementById("registerForm").reset();
				Ext.getStore("Calendars").load();
				Ext.getStore("Events").load();
			}
			else
			{
				Ext.getElementById("loginForm").reset();
				Ext.getElementById("message").innerHTML = data.message;
			}
		},
		failure: function(response) {
			
			console.log("Failure", response);
		}
	});
});
