// Demo Extensible Calendar App
// Created for CSE 330S by Shane Carr
// Washington University in St. Louis
 
 
// Ext.Loader.setConfig({
//     enabled: true,
//     disableCaching: false,
//     paths: {
//         Extensible: "http://ext.ensible.com/deploy/dev",
//         Core: "/app/core",
//         "Extensible.example": "http://ext.ensible.com/deploy/dev/examples/"
//     }
// });

// Ext.JS wants you to use `Ext.define` when making a custom component.
Ext.define("CSE330.Calendar", {
 
	// List the classes we use inside this component.  This enables Ext.JS to load
	// the files containing the definitions for these classes more efficiently.
	requires: [],
 
	// Main code for our component
	constructor: function(){
		// Set up the calendar store
		this.calendarStore = Ext.create("Extensible.calendar.data.MemoryCalendarStore", {
			autoLoad: true,
			storeId: "Calendars",
			proxy: {
				type: "rest",
				url: "/cal/",
				startParam: "offset",
				reader: {
					type: "json",
					root: "objects"
				},
				writer: {
					type: "json",
					nameProperty: "mapping"
				}
			}
		});
 
		// Set up the event store
		this.eventStore = Ext.create("Extensible.calendar.data.MemoryEventStore", {
			autoLoad: true,
			storeId: "Events",
			proxy: {
				type: "rest",
				url: "/event/",
				startParam: "offset",
				reader: {
					type: "json",
					root: "objects",
				},
				writer: {
					type: "json",
					nameProperty: "mapping"
				}
			},
			// listeners: {
			// 	'write': function(store, operation){
			// 		var title = Ext.value(operation.records[0].data[Extensible.calendar.data.EventMappings.Title.name], '(No title)');
			// 		switch(operation.action){
			// 			case 'create': 
			// 				Extensible.example.msg('Add', 'Added "' + title + '"');
			// 				break;
			// 			case 'update':
			// 				Extensible.example.msg('Update', 'Updated "' + title + '"');
			// 				break;
			// 			case 'destroy':
			// 				Extensible.example.msg('Delete', 'Deleted "' + title + '"');
			// 				break;
			// 		}
			// 	}
			// }

		});
 
		// Set up the panel (view)
		this.container = Ext.create('Extensible.calendar.CalendarPanel', {
			renderTo: Ext.get("calbox"),
			title: "My CSE330 Calendar",
			width: "100%",
			height: "100%",
			id: "ext-calendar-main",
			eventStore: this.eventStore,
			calendarStore: this.calendarStore,
			monthViewCfg: {
				showHeader: true // show the days of the week
			}
		});

		this.list = Ext.create("Ext.grid.Panel", {
			width: 400,
			height: 200,
			renderTo: Ext.getBody(),
			store: Ext.getStore("Calendars"),
			columns: [{text: "Calendar list", flex: 1, dataIndex: "Title"}]
		});
 
	}
});
 
Ext.onReady(function(){
	Ext.create("CSE330.Calendar");
});