Ext.fly("addCalForm").on("submit", function(event, element){
	event.preventDefault();
	Ext.Ajax.request({
		form: element,
		url: '/add_cal/',
		success: function(response) {
			console.log("Success", response);
			var data = JSON.parse(response.responseText);
			if(data.success)
			{
				Ext.getElementById("message").innerHTML = data.message;
				Ext.getElementById("addCalForm").reset();
				Ext.getStore("Calendars").load();
				Ext.getStore("Events").load();
			}
			else
			{
				Ext.getElementById("addCalForm").reset();
				Ext.getElementById("message").innerHTML = data.message;
			}
		},
		failure: function(response) {
			
			console.log("Failure", response);
		}
	});
});
