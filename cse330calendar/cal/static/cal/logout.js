Ext.fly("logoutForm").on("submit", function(event, element){
	event.preventDefault();
	Ext.Ajax.request({
		form: element,
		url: '/logout/',
		success: function(response) {
			console.log("Success", response);
			var data = JSON.parse(response.responseText);
			if(data.success)
			{
				Ext.fly("loginForm").show();
				Ext.fly("registerForm").show();
				Ext.fly("logoutForm").hide();
				Ext.fly("addCalForm").hide();
				Ext.getElementById("user").innerHTML = "";
				Ext.getStore("Events").loadData([], false);
				Ext.getStore("Calendars").loadData([], false);
				Ext.getStore("Calendars").load();
				Ext.getStore("Events").load();
			}
			else
			{
				alert(data.error);
			}
		},
		failure: function(response) {
			console.log("Failure", response);
		}
	});
});
