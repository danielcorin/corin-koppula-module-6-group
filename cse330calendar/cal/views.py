from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.utils import simplejson
from models import Cal, Event
import re
from django.contrib.auth.models import User

from random import randint

def index(request):
	return render(request, 'cal/index.html', {})

# def login_view(request):
# 	if request.is_ajax():
# 		requser = request.POST['username']
# 		reqpass = request.POST['password']
# 		user = authenticate(username=requser, password=reqpass)
# 		if user is not None:
# 			if user.is_active:
# 				login(request, user)
# 				# Return a success message
# 				return TemplateResponse(request, 'cal/logout.html', {})
			
# 			template = 'results.html'
# 			data = {
# 				'results': results,
# 			}
# 			return render_to_response(template, data, 
# 				context_instance = RequestContext(request))

def login_view(request):
	if request.method == 'POST':
		requser = request.POST['username']
		reqpass = request.POST['password']
		user = authenticate(username=requser, password=reqpass)
		if user is not None:
			if user.is_active:
				login(request, user)
				# Return a success message
				to_json = {
					"success": True,
					"username": user.username,
					# "events": Event.objects.all().filter(user=user.id).get()

				}
			else:
				to_json = {
					"success": False,
					"error": "this account is disabled!"
				}
				
		else:
			to_json = {
				"success": False,
				"message": "username or password invalid"
			}
	else:
		to_json = {
			"success": False,
			"message": "not a post"
		}
		

	return HttpResponse(simplejson.dumps(to_json), mimetype='application/json')

def logout_view(request):
	to_json = {
		"success": True,
	}
	logout(request)

	return HttpResponse(simplejson.dumps(to_json), mimetype='application/json')
	# return TemplateResponse(request, 'login.html', {})
	# return HttpResponse("Logged out", content_type="text/plain")

def register_view(request):
	message = "Unset"

	username = request.POST['username']
	to_json = {
		"success": True,
		"message": message,
		"username": username,
	}
	email = request.POST['email']
	password = request.POST['password']
	if not request.method == "POST":
		message = "Not a post."
		to_json["success"] = False
		to_json["message"] = message
	
	elif not re.match("^[a-zA-Z0-9_.-]+$", username):
		to_json["success"] = False
		to_json["message"] = "Invalid username."
	
	elif User.objects.filter(username=username).count():
		message = "Username %s already exists." % username
		to_json["success"] = False
		to_json["message"] = message

	elif password !=  request.POST['confirm_password']:
		to_json["success"] = False
		to_json["message"] = "Passwords do not match."

	else:
		user = User.objects.create_user(username, email, password)
		user.save()
		cal = Cal()
		cal.title = "%s's first calendar" % username
		cal.description = "%s's first calendar" % username
		cal.color = randint(1,32)
		cal.user = user
		cal.save()
		login_view(request)


	return HttpResponse(simplejson.dumps(to_json), mimetype='application/json')


def add_cal_view(request):
	to_json = {
	"success": True,
	"message": "Calendar added",
	}

	if request.method == 'POST':
		cal = Cal()
		cal.title = request.POST['title']
		cal.description = request.POST['description']
		cal.color = randint(1,32)
		cal.user = request.user
		cal.save()
	else:
		to_json["success"] = False

	return HttpResponse(simplejson.dumps(to_json), mimetype='application/json')




